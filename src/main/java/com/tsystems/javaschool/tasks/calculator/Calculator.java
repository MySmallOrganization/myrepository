package com.tsystems.javaschool.tasks.calculator;
import java.util.regex.*;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        if (statement.length() == 0){
            return null;
        }

        Pattern pat;
        Matcher mat;

        pat = Pattern.compile("[^0-9+\\-*/.()]");
        mat = pat.matcher(statement);
        if (mat.matches()) {
            return null;
        }

        pat = Pattern.compile(".{2,}");
        mat = pat.matcher(statement);
        if (mat.matches()) {
            return null;
        }

        int i=0, j=0;
        char[] statmass = new char[statement.length()];
        statement.getChars(0, (statement.length()-1), statmass, 0);
        for (char a: statmass) {
            if (a == '(') {
                i++;
                continue;
            }
            if (a == ')') {
                j++;
                if (j > i){
                    return null;
                }
            }
        }
        if (i != j) {
            return null;
        }



/*
        i = statement.lastIndexOf('(');
        j = statement.indexOf(')');
        if (i > 0) {
            brackets(statement.substring(i+1, j-1));
        }
*/
        return statement;
    }

    private static String brackets (String subbr) {
        Pattern pat;
        pat = Pattern.compile("[+\\-*/]");
        String[] num = pat.split(subbr);
        pat = Pattern.compile("[0-9.]");
        String[] op = pat.split(subbr);
        String[] expr = new String[num.length + op.length];
        int i, j=0, k=0;
        for (i=0; i < expr.length; i++) {
            if (i % 2 == 0){
                expr[i] = num[j];
                j++;
            }
            else {
                expr[i] = op[k];
                k++;
            }
        }
        return subbr;

    }

}
