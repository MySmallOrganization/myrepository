package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;
import static java.util.stream.Collectors.toList;

public class Subsequence {

    public static void main(String[] args) {
        List x = Stream.of(3, 9, 1, 5, 7).collect(toList());
        List y = Stream.of(1, 2, 3, 4, 5, 7, 9, 20).collect(toList());
        try {
            Subsequence subsequence = new Subsequence();
            boolean b = subsequence.find(x, y);
            System.out.println(b);
        } catch (IllegalArgumentException e){
            System.out.println(e);
        }
    }
    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public  boolean find(List x, List y) throws IllegalArgumentException {

        if (x == null || y == null){
            throw new IllegalArgumentException();
        }

        int x_s = x.size();
        int y_s = y.size();
        int i=0;
        if (y_s < x_s){
            return false;
        }
        if (x_s == 0){
            return true;
        }

        Iterator<Object> iterator_x = x.iterator();
        Iterator<Object> iterator_y = y.iterator();

        Object a, b;

        while (iterator_x.hasNext()){
            a = iterator_x.next();
            while (iterator_y.hasNext()) {
                b = iterator_y.next();
                if (a.equals(b)) {
                    i++;
                    break;
                }
            }
        }
        if (i == x_s){
            return true;
        }
        return false;
    }
}
