package com.tsystems.javaschool.tasks.pyramid;

import com.sun.org.apache.xpath.internal.SourceTree;

import javax.xml.bind.SchemaOutputResolver;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
//import com.tsystems.javaschool.tasks.pyramid;

public class PyramidBuilder {

    public static void main(String[] args) {
//         PyramidBuilderTest pyramidBuilderTest = new PyramidBuilderTest();
        List<Integer> input = Arrays.asList(1, 3, 2, 0, 4, 5);
        try {
            int[][] pyr = buildPyramid(input);
            for (int[] a : pyr) {
                for (int b : a) {
                    System.out.print("  " + b);
                }
                System.out.println();
            }
        } catch (CannotBuildPyramidException e){
            System.out.println(e);
        }

    }
    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public static int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        int n_el = inputNumbers.size();
        if (n_el == 0){
            throw new CannotBuildPyramidException();
        }

        for (Integer elem: inputNumbers){
            if (elem == null){
                throw new CannotBuildPyramidException();
            }
        }

        int n_lay, n_line;
        int i, j, k;
        i = n_el;
        for(n_lay=1; i>0; n_lay++){
            i-=n_lay;
        }
        if(i != 0){
            throw new CannotBuildPyramidException();
        }

        n_lay-=1;
        n_line=2*n_lay-1;
        int[] inp = new int[n_el];
        i = 0;
        for (Integer a: inputNumbers){
            inp[i] = a;
            i++;
        }

        Arrays.sort(inp);

        int[][] pyr = new int[n_lay][n_line];

        k = 0;
        for(i=0; i<n_lay; i++){
            for(j=n_lay-i-1; j<=n_lay+i-1; j+=2){
                pyr[i][j] = inp[k];
                k++;
            }
        }

        return pyr;
    }


}
